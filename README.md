# Project Information

## Prerequisites

-   Docker engine
-   Docker compose
-   Example domain name is `wordpress.local`
-   Environment summary

    ```yaml
    project:
        - project-name: "Wordpress"
        - magento-version: latest
    services:
        - nginx: 1.18
        - php: 7.4
        - mysql: 8.0
    ```

## Installation

### Initialize our project

```bash
cd `<working dir>`
git clone git@gitlab.com:muoido1/wordpress-docker.git
cp .env-mac .env # for macos
cp .env-linux .env # for linux

Change domain and wordpres info in env/wp.env

Default:
WP_TITLE="wordpress docker"
WP_ADMIN_USER=admin
WP_ADMIN_PASSWORD=12345678
WP_ADMIN_EMAIL=muoido@coffeemugvn.com
DOMAIN="wordpress.docker"

./bin/setup

```
### Other tips

- ERR_CERT_INVALID in Chrome on MacOS: Just make sure the page is selected (click anywhere on the background), and type `thisisunsafe`

## Custom CLI Commands

- `bin/bash`: Drop into the bash prompt of your Docker container. The `phpfpm` container should be mainly used to access the filesystem within Docker.
- `bin/cli`: Run any CLI command without going into the bash prompt. Ex. `bin/cli ls`
- `bin/clinotty`: Run any CLI command with no TTY. Ex. `bin/clinotty chmod u+x bin/magento`
- `bin/cliq`: The same as `bin/cli`, but pipes all output to `/dev/null`. Useful for a quiet CLI, or implementing long-running processes.
- `bin/copyfromcontainer`: Copy folders or files from container to host. Ex. `bin/copyfromcontainer vendor`
- `bin/copytocontainer`: Copy folders or files from host to container..env.sample Ex. `bin/copytocontainer --all`
- `bin/cron`: Start or stop the cron service. Ex. `bin/cron start`
- `bin/down`: docker-compose down all containters
- `bin/wp-core-download`: Download  wordpress version  to `/var/www/html` directory within the container. 
- `bin/fixowns`: This will fix filesystem ownerships within the container.
- `bin/fixperms`: This will fix filesystem permissions within the container.
- `bin/grunt`: Run the grunt binary. Ex. `bin/grunt exec`
- `bin/mysql`: Run the MySQL CLI with database config from `env/db.env`. Ex. `bin/mysql -e "EXPLAIN core_config_data"` or`bin/mysql < wordpress.sql`
- `bin/mysqldump`: Backup the wordpress database. Ex. `bin/mysqldump > wordpress.sql`
- `bin/pwa-studio`: (BETA) Start the PWA Studio server. Note that Chrome will throw SSL cert errors and not allow you to view the site, but Firefox will.
- `bin/remove`: Remove all containers.
- `bin/removeall`: Remove all containers, networks, volumes, and images.
- `bin/removevolumes`: Remove all volumes.
- `bin/restart`: Stop and then start all containers.
- `bin/root`: Run any CLI command as root without going into the bash prompt. Ex `bin/root apt-get install nano`
- `bin/rootnotty`: Run any CLI command as root with no TTY. Ex `bin/rootnotty chown -R app: /var/www/html`
- `bin/setup`: Run the Wordpress setup process to install Wordpress from the source code, with optional domain name. Defaults to `wordpress.local`. 
- `bin/setup-domain`: Setup wordress domain name. Ex: `bin/setup-domain wordpress.local`
- `bin/setup-grunt`: Install and configure Grunt JavaScript task runner to compile .less files
- `bin/setup-ssl`: Generate an SSL certificate for one or more domains. Ex. `bin/setup-ssl wordpress.local`
- `bin/setup-ssl-ca`: Generate a certificate authority and copy it to the host.
- `bin/start`: Start all containers, good practice to use this instead of `docker-compose up -d`, as it may contain additional helpers.
- `bin/status`: Check the container status.
- `bin/stop`: Stop all containers.
- `bin/xdebug`: Disable or enable Xdebug. Accepts params `disable` (default) or `enable`. Ex. `bin/xdebug enable`
- `bin/wpcli`: Install wp-cli on container.
- `bin/wp-core-download`: Download wordpress core
## Misc Info


### Database



You can use the `bin/mysql` script to import a database, for example a file stored in your local host directory at `wordpress.sql`:

```bash
bin/mysql  wordpress.sql
```

You also can use `bin/mysqldump` to export the database. The file will appear in your local host directory at `wordpress.sql`:

```bash
bin/mysqldump wordpress.sql

### Email / Mailcatcher

View emails sent locally through Mailcatcher by visiting [http://{yourdomain}:1080](http://{yourdomain}:1080). During development, it's easiest to test emails using a third-party module such as [https://github.com/mageplaza/magento-2-smtp](Mageplaza's SMTP module). Set the mailserver host to `mailcatcher` and port to `1080`.


You may also monitor Redis by running: `bin/redis redis-cli monitor`

For more information about Redis usage with Magento, <a href="https://devdocs.magento.com/guides/v2.4/config-guide/redis/redis-session.html" target="_blank">see the DevDocs</a>.

### Xdebug & VS Code

Install and enable the PHP Debug extension from the [Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-debug).

Otherwise, this project now automatically sets up Xdebug support with VS Code. If you wish to set this up manually, please see the [`.vscode/launch.json`](https://github.com/markshust/docker-magento/blame/master/compose/.vscode/launch.json) file.

### Xdebug & PhpStorm

1. First, install the [Chrome Xdebug helper](https://chrome.google.com/webstore/detail/xdebug-helper/eadndfjplgieldjbigjakmdgkmoaaaoc). After installed, right click on the Chrome icon for it and go to Options. Under IDE Key, select PhpStorm from the list to set the IDE Key to "PHPSTORM", then click Save.

2. Next, enable Xdebug debugging in the PHP container by running: `bin/xdebug enable`.

3. Then, open `PhpStorm > Preferences > PHP` and configure:

    * `CLI Interpreter`
        * Create a new interpreter from the `From Docker, Vagrant, VM...` list.
        * Select the Docker Compose option.
        * For Server, select `Docker`. If you don't have Docker set up as a server, create one and name it `Docker`.
        * For Configuration files, add both the `docker-compose.yml` and `docker-compose.dev.yml` files from your project directory.
        * For Service, select `phpfpm`, then click OK.
        * Name this CLI Interpreter `phpfpm`, then click OK again.

    * `Path mappings`
        * There is no need to define a path mapping in this area.
   ![CLI Interperter](https://i.imgur.com/qhuLxAc.jpg)

4. Open `PhpStorm > Preferences > PHP > Debug` and ensure Debug Port is set to `9000,9003`
   ![Xdebug Port](https://i.imgur.com/BKljp9u.jpg)

5. Open `PhpStorm > Preferences > PHP > Servers` and create a new server:

    * For the Name, set this to the value of your domain name (ex. `magento.test`).
    * For the Host, set this to the value of your domain name (ex. `magento.test`).
    * Keep port set to `80`.
    * Check the "Use path mappings" box and map `src` to the absolute path of `/var/www/html`.

   ![Xdebug Server](https://i.imgur.com/BmHbvvw.jpg)

6. Go to `Run > Edit Configurations` and create a new `PHP Remote Debug` configuration.

    * Set the Name to the name of your domain (ex. `magento.test`).
    * Check the `Filter debug connection by IDE key` checkbox, select the Server you just setup.
    * For IDE key, enter `PHPSTORM`. This value should match the IDE Key value set by the Chrome Xdebug Helper.
    * Click OK to finish setting up the remote debugger in PHPStorm.

   ![PHP Remote Debug](https://i.imgur.com/7J7bKcC.jpg)

7. Open up `pub/index.php` and set a breakpoint near the end of the file.

    * Start the debugger with `Run > Debug 'magento.test'`, then open up a web browser.
    * Ensure the Chrome Xdebug helper is enabled by clicking on it and selecting Debug. The icon should turn bright green.
    * Navigate to your Magento store URL, and Xdebug should now trigger the debugger within PhpStorm at the toggled breakpoint.



